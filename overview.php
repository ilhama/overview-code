    public function getCompany($empId) {
        $empId = 'TBS16101858';
        $dataIsAdmin = DB::table('THRMEMAUSERAUTORIZATION')
                    ->where('emp_id',$empId)
                    ->select("isadmin","LSTCOSTCENTER")
                    ->first();
        // dd($empId, $dataIsAdmin, $dataIsAdmin == null);
        if ($dataIsAdmin == null) {
            return [];
        }else{
            $isAdminStatus = $dataIsAdmin->isadmin;
            $dataCompany = [];
            if ($isAdminStatus == 0) {
                $lstcostcenter = $dataIsAdmin->lstcostcenter;
                $dataCompany = DB::select("SELECT
                    DISTINCT(COSTCENTER_CODE) AS code,
                    COSTCENTER_ALIAS AS alias,
                    COSTCENTER_NAME AS name, 
                    COMPANY_CODE_ALIAS AS groupCode, 
                    LOB AS lobCode,
                    COMPANY_ID AS companyID,
                    CITY_NAME AS cityName,
                    COSTCENTER_LEVEL AS cclevel
                FROM
                    VIEW_EMP_PRODUCTIVITY
                WHERE
                    COSTCENTER_CODE IN (
                        SELECT
                            GROUP_CODE
                        FROM
                            VIEW_EMP_PRODUCTIVITY
                        GROUP BY
                            GROUP_CODE
                    )
                    AND COSTCENTER_LEVEL != 4
                    AND COSTCENTER_CODE IN (
                        $lstcostcenter
                    )
                ORDER BY
                    -- COMPANY_ID ASC,
                    COSTCENTER_NAME ASC");
            }else if ($isAdminStatus == 1) {
                $dataCompany = DB::select("SELECT
                    DISTINCT(COSTCENTER_CODE) AS code,
                    COSTCENTER_ALIAS AS alias,
                    COSTCENTER_NAME AS name, 
                    COMPANY_CODE_ALIAS AS groupCode, 
                    LOB AS lobCode,
                    COMPANY_ID AS companyID,
                    CITY_NAME AS cityName,
                    COSTCENTER_LEVEL AS cclevel
                FROM
                    VIEW_EMP_PRODUCTIVITY
                WHERE
                    COSTCENTER_CODE IN (
                        SELECT
                            GROUP_CODE
                        FROM
                            VIEW_EMP_PRODUCTIVITY
                        GROUP BY
                            GROUP_CODE
                    )
                    AND COSTCENTER_LEVEL != 4
                ORDER BY
                    -- COMPANY_ID ASC,
                    COSTCENTER_NAME ASC");
            }
            
            
            // dd($dataCompany);
            foreach ($dataCompany as $key => $value) {
                $branchData = $this->getBranchCompany($value->code);

                if ($dataCompany[$key]->cclevel == 3) {
                    $dataCompany[$key]->code =  $dataCompany[$key]->code."-Console";
                    $dataCompany[$key]->name =  $dataCompany[$key]->name." - Console";
                    $dataCompany[$key]->namecity = $value->name;
                    $dataCompany[$key]->branch = [];
                } else if ($dataCompany[$key]->cclevel == 2){
                    $dataCompany[$key]->branch = [];
                    $dataCompany[$key]->namecity = $value->name.(!empty($value->cityname) ? ' ('.$value->cityname.')' : '');
                }
                else{
                    $dataCompany[$key]->branch = $branchData;
                    $dataCompany[$key]->namecity = $value->name.(!empty($value->cityname) ? ' ('.$value->cityname.')' : '');
                }
            }
            return $dataCompany;
        }

    }
